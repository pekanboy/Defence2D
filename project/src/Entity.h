//
// Created by Nikita on 27.04.2020.
//

#ifndef DEFENCE2D_ENTITY_H
#define DEFENCE2D_ENTITY_H

#include <iostream>
#include <SFML/Graphics.hpp>

class Entity {
public:
    int frame = 0;  // Переменная для контроля смещений спрайта
    int dir = 0;
    float getX() const;
    float getY() const;
    float getHeight() const;
    float getWeight() const;
    float getSpeed() const;
    bool entityCollision{};
    bool setCollision(bool setting);
    void setPosition(float x, float y);
    sf::FloatRect getRect() const;
    std::string getName() const;
    float getAttack() const;
    sf::Texture& getTexture();
    sf::Sprite& getSprite();
    float getRate() const;
    float getHealth() const;
    void setHP(float HP);
    void update(float time, float speed);
    void Setup(float x, float y, float h, float w, float speed, float attack, float rate, float health, const std::string &name);

    bool canW = true;
    bool canA = true;
    bool canS = true;
    bool canD = true;

    virtual ~Entity() {}
protected:
    std::string m_name;  // Путь к текстуре
    sf::Texture m_texture;
    sf::Sprite m_sprite;
    float m_x;
    float m_y;
    float m_height;
    float m_weight;
    float m_speed;
    float m_attack;
    float m_rate;
    float m_health;
};

#endif //DEFENCE2D_ENTITY_H
