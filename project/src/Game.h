//
// Created by aleksey on 26.04.2020.
//

#ifndef DEFENCE2D_GAME_H
#define DEFENCE2D_GAME_H

#include <list>
#include "Window.h"
#include "Player.h"
#include "Level.h"
#include "Enemy.h"
#include "ObjectEventManager.h"
#include "weapon.h"
#include "BulletAndGuns.h"

#define NEWGAME -1;

class Game{
public:
    Game(myWindow &window);
    int Update();
    void Render();
    void VSync();
    myWindow* GetWindow();
    sf::Time GetElapsed();
    Level *GetMap();
    Player *GetPlayer();
    void RestartClock();
    void LoadMap();
    void BaseCamera();
    void SetCam ();
    void MoveCam();
    void Fire(float attack);
    void FireEnemy(float x, float y, float attack);
    int checkCollision();
    int menu(RenderWindow & window, int &key);
    void Text();
    void EndText();
    float maxWeaponHp = 0;
private:
    void MoveHero(float frametime);
    void MoveEnemy(float frametime);
    void MoveBullet(float frametime);
    void CollisionPro(Entity& entity);
    Player hero;
    std::list<Enemy> m_enemy;
    std::list<Weapon> m_weapon;
    std::list<Weapon> m_pharmace;
    std::list<Bullet> m_bullet;
    std::list<Bullet> m_bulletEnemy;
    Level map;
    CameraMan PlayerCamera;
    sf::Clock m_clock;
    sf::Time m_elapsed;
    sf::Time m_period;
    sf::Time m_beforeWave;
    bool m_isWave = false;
    myWindow &m_window;
    MapEvent events;

    int waveCount;
    int counterSpawn = 0;

    void updateBulletEnemy(float frametime);

    void TextWave();
};

#endif //DEFENCE2D_GAME_H
