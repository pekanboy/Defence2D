//
// Created by Nikita on 26.04.2020.
//

#ifndef DEFENCE2D_PLAYER_H
#define DEFENCE2D_PLAYER_H

#include <list>
#include <memory>
#include "Entity.h"
#include "Window.h"
#include "weapon.h"

class Player : public Entity {
public:
    Player() = default;
    int viewDir = 0;
    bool active = false;

    float Move(Level &level);
    bool View(const myWindow &window);
    Weapon& getActive();
    void setActive(Weapon &weapon);
    void throwItem();
    int DistBull = 0;
private:
    Weapon ActiveItem;
};

#endif //DEFENCE2D_PLAYER_H
