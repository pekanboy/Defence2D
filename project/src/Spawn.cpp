//
// Created by aleksey on 14.05.2020.
//

#include <list>
#include <SFML/System/Time.hpp>
#include "Spawn.h"
#include "ConfigurationClass.h"
#include "ObjectEventManager.h"

void Spawn::SpawnEnemy(std::list<Enemy>& m_enemy, Level& map, int *waveCount, MapEvent &events) {
    std::vector<SpriteInfo> spriteEnemy = getEnemyInfo(WAVENUM[*waveCount]);
    m_enemy.clear();
    *waveCount += 1;
    int size = spriteEnemy.size();
    if (size == 0) {
        std::cout << "The waves are over" << std::endl;
        *waveCount = -1;
        return;
    }
    srand(time(nullptr));
    m_enemy.resize(size);
    int i = 0;
    for (auto it = m_enemy.begin(); it != m_enemy.end(); std::advance(it,1)) {
        it->Setup(rand() % map.getWidth(), rand() % 3000, spriteEnemy[i].height, spriteEnemy[i].weight,
                  spriteEnemy[i].speed, spriteEnemy[i].attack, spriteEnemy[i].rate, spriteEnemy[i].health,
                  spriteEnemy[i].placeSprite);
        ++i;
        events.MapEventActivator(map, *it);
    }
}

void Spawn::SpawnWeapon(std::list<Weapon>& m_weapon, Level& map, const int *waveCount, MapEvent &events) {
    std::vector<SpriteInfo> spriteItems = getItemInfo(WAVENUM[*waveCount]);
    m_weapon.clear();
    int size = spriteItems.size();
    if (size == 0) {
        std::cout << "The items are over" << std::endl;
        return;
    }
    srand(time(nullptr));
    m_weapon.resize(size);
    int i = 0;
    for (auto it = m_weapon.begin(); it != m_weapon.end(); std::advance(it, 1)) {
        it->Setup(rand() % map.getWidth(), rand() % map.getHeight(), spriteItems[i].height, spriteItems[i].weight,
                  spriteItems[i].speed, spriteItems[i].attack, spriteItems[i].rate, spriteItems[i].health,
                  spriteItems[i].placeSprite);
        ++i;
        events.MapEventActivator(map, *it);
    }
}

void Spawn::SpawnPharmace(std::list<Weapon> &m_enemy, Level &map, MapEvent &events) {
    std::vector<SpriteInfo> spriteItems = getPharmaceInfo();
    m_enemy.clear();
    int size = spriteItems.size();
    if (size == 0) {
        std::cout << "The items are over" << std::endl;
        return;
    }
    srand(time(nullptr) / 2);
    m_enemy.resize(size);
    int i = 0;
    for (auto it = m_enemy.begin(); it != m_enemy.end(); std::advance(it,1)) {
        it->Setup(rand() % map.getWidth(), rand() % map.getHeight(), spriteItems[i].height, spriteItems[i].weight,
                  0, 0, 0, spriteItems[i].health, spriteItems[i].placeSprite);
        ++i;
        events.MapEventActivator(map, *it);
    }
}
