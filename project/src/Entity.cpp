#include <iostream>
#include "Entity.h"

float Entity::getX() const{
    return m_x;
}

float Entity::getY() const{
    return m_y;
}

float Entity::getWeight() const{
    return m_weight;
}

float Entity::getHeight() const{
    return m_height;
}

sf::FloatRect Entity::getRect() const{
    sf::FloatRect playerRect;
    playerRect = m_sprite.getGlobalBounds();
    return playerRect;
};

bool Entity::setCollision(bool setting) {
    entityCollision = setting;
}

void Entity::update(float time, float speed) {
    float m_vx;
    float m_vy;

    switch (dir) {
        case 0:
            m_vx = 0;
            m_vy = speed;
            break;
        case 1:
            m_vx = -speed;
            m_vy = 0;
            break;
        case 2:
            m_vx = speed;
            m_vy = 0;
            break;

        case 3:
            m_vx = 0;
            m_vy = -speed;
            break;
    }

    m_x += m_vx * time;
    m_y += m_vy * time;
    m_sprite.setPosition(m_x, m_y);
}

std::string Entity::getName() const{
    return m_name;
}

sf::Texture& Entity::getTexture() {
    return m_texture;
}

sf::Sprite& Entity::getSprite() {
    return m_sprite;
}

float Entity::getSpeed() const{
    return m_speed;
}

void Entity::Setup(float x, float y, float h, float w, float speed, float attack, float rate, float health, const std::string &name) {
    m_x = x;
    m_y = y;
    m_weight = w;
    m_height = h;
    m_speed = speed;
    m_name = name;
    m_attack = attack;
    m_health = health;
    m_rate = rate;
    sf::Image m_image;
    m_image.loadFromFile(m_name);
    m_texture.loadFromImage(m_image);
    m_sprite.setTexture(m_texture);
    m_sprite.setTextureRect(sf::IntRect(0, 0, m_weight, m_height));
    m_sprite.setPosition(m_x, m_y);

}

void Entity::setPosition(float x, float y) {
    m_x = x;
    m_y = y;
}

float Entity::getAttack() const {
    return m_attack;
}

float Entity::getRate() const {
    return m_rate;
}

float Entity::getHealth() const {
    return m_health;
}

void Entity::setHP(float HP) {
    m_health = HP;
}
