//
// Created by aleksey on 26.04.2020.
//

#ifndef DEFENCE2D_COMMON_H
#define DEFENCE2D_COMMON_H

#define HEADER_NAME "Defence2D"

#define KEY_CONFIG "../Configurations/keys.xml"
#define SPRITE_CONFIG "../Configurations/spriteInfo.xml"
#define MAP_EVENT_CONFIG "../Configurations/mapConfig.xml"
#define MAP_TILESET_FILE "../Map/tileset.png"
#define PERIOD 30

#define MAP_PLACE "../Map/test.tmx"

static const char *WAVENUM[] = {"one",
                                "two",
                                "three",
                                "four",
                                "five",
                                "six",
                                nullptr};

#endif //DEFENCE2D_COMMON_H
