//
// Created by felixdev on 28.04.2020.
//
#include <iostream>
#include "Level.h"
#include "Collision.h"

bool Collision (sf::FloatRect body, std::vector<Object> &objectscol) {
    for (int i = 0; i < objectscol.size(); i++) {
        if(body.intersects(objectscol[i].rectObj)) {
            return true;
        }
    }
    return false;
}

bool CollisionEntity(Entity &one, Entity &two) {
    return one.getRect().intersects(two.getRect());
}