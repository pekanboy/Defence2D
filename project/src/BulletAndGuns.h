//
// Created by felixdev on 11.05.2020.
//
#ifndef DEFENCE2D_BULLETANDGUNS_H
#define DEFENCE2D_BULLETANDGUNS_H
#include "Entity.h"
#include "ConfigurationClass.h"
class Bullet : public Entity {
private:
    int damage;
public:
    float sin;
    float cos;
    Bullet();
    Bullet(float x, float y, float h, float w, float speed, float attack, float rate, float health, const sf::String &name);
    Bullet& operator=(const Bullet &entity);
};
#endif //DEFENCE2D_BULLETANDGUNS_H
