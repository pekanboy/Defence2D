//
// Created by aleksey on 24.04.2020.
//

#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include "Window.h"

myWindow::myWindow() { Setup("myWindow", sf::Vector2u(1980,1240)); }

myWindow::myWindow(const std::string& l_title, sf::Vector2<unsigned int> l_size) {
    Setup(l_title, l_size);
}

myWindow::~myWindow() { Destroy(); }

void myWindow::Create() {
    m_window.create({m_windowSize.x, m_windowSize.y, 32},
                    m_windowTitle, sf::Style::Default);
}

void myWindow::VSync () {
    m_window.setVerticalSyncEnabled(true);
}
void myWindow::Destroy() {
    m_window.close();
}

void myWindow::Update(){
    sf::Event event;
    while(m_window.pollEvent(event)){
        if(event.type == sf::Event::Closed){
            m_isDone = true;
        }
    }
}

void myWindow::BeginDraw(){ m_window.clear(sf::Color::Black); }
void myWindow::EndDraw(){ m_window.display(); }

bool myWindow::IsDone() { return m_isDone; }

sf::Vector2u myWindow::GetWindowSize() { return m_windowSize; }

void myWindow::Draw(sf::Sprite l_drawable) {
    m_window.draw(l_drawable);
}
void myWindow::DrawMap (std::vector<Layer> layers) {
    for(int layer = 0; layer < layers.size(); layer++)
        for(int tile = 0; tile < layers[layer].tiles.size(); tile++)
            m_window.draw(layers[layer].tiles[tile]);
}
void myWindow::Setup(const std::string &l_title, const sf::Vector2u& l_size) {
    m_windowTitle = l_title;
    m_windowSize = l_size;
    m_isDone = false;
    Create();
}

void myWindow::BaseView(CameraMan PlayerCamera) {
    m_window.setView(PlayerCamera.GetPlayerCamera());
}
void myWindow::Close(){ m_isDone = true; }
