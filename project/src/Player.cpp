#include <iostream>
#include <utility>
#include "Player.h"
#include "Window.h"

float Player::Move(Level &map) {
    bool IsKeyPressed = false;
    bool Move = false;
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
        if (canS && m_y < map.getHeight() - m_height) {
            dir = 0;
            IsKeyPressed = true;
        } else {
            Move = true;
        }
        viewDir = 0;

    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
        if (canA && m_x > 0) {
            dir = 1;
            IsKeyPressed = true;
        } else {
            Move = true;
        }
        viewDir = 1;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
        if (canD && m_x < map.getWidth() - m_weight) {
            dir = 2;
            IsKeyPressed = true;
        } else {
            Move = true;
        }
        viewDir = 2;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
        if (canW && m_y > 0) {
            dir = 3;
            IsKeyPressed = true;
        } else {
            Move = true;
        }
        viewDir = 3;
    }
    if (!IsKeyPressed) {
        if (!Move) {
            frame = 0;
        }
        return 0;
    } else {
        return m_speed;
    }
}

bool Player::View(const myWindow &window) {
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
        viewDir = 0;
        return true;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
        viewDir = 1;
        return true;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
        viewDir = 2;
        return true;
    }
    if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
        viewDir = 3;
        return true;
    }
    return false;
}

Weapon& Player::getActive() {
    return ActiveItem;
}

void Player::setActive(Weapon &weapon) {
    ActiveItem = weapon;
    active = true;
}

void Player::throwItem() {
    active = false;
}