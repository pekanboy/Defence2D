//
// Created by aleksey on 14.05.2020.
//

#ifndef DEFENCE2D_SPAWN_H
#define DEFENCE2D_SPAWN_H

#include <list>
#include "Enemy.h"
#include "Level.h"
#include "weapon.h"
#include "ObjectEventManager.h"

class Spawn {
public:
    static void SpawnEnemy(std::list<Enemy>& m_enemy, Level& map, int *waveCount, MapEvent &events);
    static void SpawnWeapon(std::list<Weapon>& m_weapon, Level& map, const int *waveCount, MapEvent &events);
    static void SpawnPharmace(std::list<Weapon>& m_enemy, Level& map, MapEvent &events);
};

#endif //DEFENCE2D_SPAWN_H
