//
// Created by Nikita on 27.04.2020.
//

#ifndef DEFENCE2D_ENEMY_H
#define DEFENCE2D_ENEMY_H

#include <SFML/Graphics.hpp>
#include "Entity.h"

class Enemy : public Entity{
public:
    Enemy();
    float move(float heroX, float heroY);
    int DistBull = 0;

    ~Enemy() override {}
};

#endif //DEFENCE2D_ENEMY_H