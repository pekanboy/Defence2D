//
// Created by aleksey on 24.04.2020.
//

#ifndef DEFENCE2D_WINDOW_H
#define DEFENCE2D_WINDOW_H


#include "Level.h"
#include "Camera.h"

class myWindow {
public:
    myWindow();
    myWindow(const std::string& l_title, sf::Vector2u l_size);
    ~myWindow();
    void BeginDraw(); // Clear the window.
    void EndDraw(); // Display the changes.
    void Update();
    void DrawMap (std::vector<Layer> layers);
    bool IsDone();
    sf::Vector2u GetWindowSize();
    void Close();
    void Draw(sf::Sprite l_drawable);
    void VSync ();
    void BaseView(CameraMan PlayerCamera);
    sf::RenderWindow m_window;

private:
    void Setup(const std::string& l_title, const sf::Vector2u& l_size);
    void Destroy();
    void Create();
    sf::Vector2u m_windowSize;
    std::string m_windowTitle;
    bool m_isDone;
};

#endif //DEFENCE2D_WINDOW_H
