//
// Created by Nikita on 27.04.2020.
//


#include <SFML/Graphics.hpp>
#include <iostream>
#include <tgmath.h>
#include "Enemy.h"

Enemy::Enemy() = default;

float Enemy::move(float heroX, float heroY) {
    float dx = m_x - heroX;
    float dy = m_y - heroY;
    float k = dy / dx;

    if (k > -1 && k < 1) {
        if (m_x > heroX ) {
            dir = 1;
        } else {
            dir = 2;
        }
    } else {
        if (m_y > heroY) {
            dir = 3;
        } else {
            dir = 0;
        }
    }

    if (std::abs(m_x - heroX) < 500 &&
        std::abs(m_y - heroY) < 500) {
        frame = 0;
        getSprite().setTextureRect(sf::IntRect(0,
                                               dir * (int) m_height,
                                               (int) m_weight,
                                               (int) m_height));
        return -1;
    }
    ++frame;

    if (frame >= 36) {
        frame -= 36;
    }
    switch (dir) {
        case 1:
            if (!canA) {
                getSprite().setTextureRect(sf::IntRect((int) m_weight * (int)(frame / 9),
                                                       dir * (int) m_height,
                                                       (int) m_weight,
                                                       (int) m_height));
                return 0;
            }
            break;
        case 2:
            if (!canD) {
                getSprite().setTextureRect(sf::IntRect((int) m_weight * (int)(frame / 9),
                                                       dir * (int) m_height,
                                                       (int) m_weight,
                                                       (int) m_height));
                return 0;
            }
            break;
        case 3:
            if (!canW) {
                getSprite().setTextureRect(sf::IntRect((int) m_weight * (int)(frame / 9),
                                                       dir * (int) m_height,
                                                       (int) m_weight,
                                                       (int) m_height));
                return 0;
            }
            break;
        case 0:
            if (!canS) {
                getSprite().setTextureRect(sf::IntRect((int) m_weight * (int)(frame / 9),
                                                       dir * (int) m_height,
                                                       (int) m_weight,
                                                       (int) m_height));
                return 0;
            }
            break;
    }
    getSprite().setTextureRect(sf::IntRect((int) m_weight * (int)(frame / 9),
                                                dir * (int) m_height,
                                                (int) m_weight,
                                                (int) m_height));
    return m_speed;
}
