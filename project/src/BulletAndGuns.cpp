//
// Created by felixdev on 11.05.2020.
//
#include "BulletAndGuns.h"

Bullet::Bullet() { }

Bullet::Bullet(float x, float y, float h, float w, float speed, float attack, float rate, float health, const sf::String &name) {
    Setup(x, y, h, w, speed, attack, rate, health, name);
}

Bullet& Bullet::operator=(const Bullet &entity) {
    if (this != &entity)
        Setup(entity.getX(), entity.getY(), entity.getHeight(), entity.getWeight(), entity.getSpeed(), entity.getAttack(), entity.getRate(), entity.getHealth(), entity.getName());
    return *this;
}