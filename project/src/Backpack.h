//
// Created by NIKITA on 17.05.2020.
//

#ifndef DEFENCE2D_BACKPACK_H
#define DEFENCE2D_BACKPACK_H

#include <SFML/Graphics.hpp>

using namespace sf;

class Backpack {
private:
    Image image;
    Texture pic;
public:
    void BackpackDraw(RenderWindow &window);
};

#endif //  DEFENCE2D_BACKPACK_H