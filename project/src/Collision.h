//
// Created by felixdev on 28.04.2020.
//

#ifndef DEFENCE2D_COLLISION_H
#define DEFENCE2D_COLLISION_H

#include "Level.h"
#include "Game.h"
using namespace sf;

bool Collision (sf::FloatRect body, std::vector<Object> &objectscol);
bool CollisionEntity(Entity &one, Entity &two);

#endif //DEFENCE2D_COLLISION_H
