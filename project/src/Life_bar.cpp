#include "Life_bar.h"

LifeBar :: LifeBar() {
    image.loadFromFile("../image/hp.png");
    image.createMaskFromColor(Color :: White);
    t.loadFromImage(image);
    s.setTexture(t);
    s.setTextureRect(IntRect(0, 0, 540, 130));

    bar.setFillColor(Color(0, 0, 0));//черный прямоугольник накладывается сверху и появляется эффект отсутствия здоровья
    max = 100;
}

void LifeBar :: HpUpdate(int k, RenderWindow &window) {
    Vector2f center = window.getView().getCenter();
    Vector2f size = window.getView().getSize();

    if (k>0) {
        if (k < max) {
            bar.setSize(Vector2f(-(max - k) * 4 + 10,
                                 40));
        }
    }
    if (k == 0) {
        bar.setSize(Vector2f(-390,
                             40));
    }
}


void LifeBar :: HpDraw(RenderWindow &window) {
    Vector2f center = window.getView().getCenter();
    Vector2f size = window.getView().getSize();

    s.setPosition(center.x - size.x / 2 + 10, center.y - size.y / 2 + 10);//позиция на экране
    bar.setPosition(center.x - size.x / 2 + 515, center.y - size.y / 2 + 65);

    window.draw(s);
    window.draw(bar);

}