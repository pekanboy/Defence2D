//
// Created by felixdev on 27.04.2020.
//

#ifndef DEFENCE2D_LEVEL_H
#define DEFENCE2D_LEVEL_H

#include <SFML/Graphics.hpp>
#include <string>
#include <vector>
#include <map>

struct Layer
{
    std::vector<sf::Sprite> tiles;
};
struct Object
{
    sf::FloatRect rectObj;
    std::string nameObj;
};

class Level {
public:
    bool LoadFromFile(std::string filename);
    std::vector<Object> GetObjectForName(std::string name);
    std::vector<Layer> layers;
    int getWidth();
    int getHeight();

private:
    int width, height, tileWidth, tileHeight;
    int firstTileID;
    sf::Rect<float> drawingBounds;
    sf::Texture tilesetImage;
    std::vector<Object> objects;
};
#endif //DEFENCE2D_LEVEL_H
