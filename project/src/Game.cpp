//
// Created by aleksey on 26.04.2020.
// Adding by Nikita on 03.05.2020
//

#include <math.h>
#include <sstream>
#include <unistd.h>
#include "Game.h"
#include "Level.h"
#include "ConfigurationClass.h"
#include "Common.h"
#include "Spawn.h"
#include "Collision.h"
#include "Backpack.h"
#include "Life_bar.h"
#include "BulletAndGuns.h"

Game::Game(myWindow &window)
        : waveCount(0), m_window(window) {
    SpriteInfo si = getPlayerInfo();
    hero.Setup(4000, 4000, si.height, si.weight, si.speed, si.attack, si.rate, si.health, si.placeSprite);
}

void Game::VSync () {
    m_window.VSync();
}
int Game::Update() {
    m_window.Update(); // Update window events.
    if (waveCount != -1) {
        if (m_enemy.empty() && counterSpawn == 0) {
            m_isWave = false;
            m_beforeWave = seconds(PERIOD);
            Spawn::SpawnWeapon(m_weapon, map, &waveCount, events);
            Spawn::SpawnPharmace(m_pharmace, map, events);
            m_period = seconds(0);
            counterSpawn = 1;
        }
        if (m_enemy.empty() && m_period > seconds(PERIOD)) {
            m_isWave = true;
            Spawn::SpawnEnemy(m_enemy, map, &waveCount, events);
            counterSpawn = 0;
        }
    }
    float frametime = 1.0f / 60.0f;
    if(m_elapsed.asSeconds() >= frametime) {
        MoveHero(frametime);
        MoveEnemy(frametime);
        MoveBullet(frametime);
        updateBulletEnemy(frametime);
        m_elapsed -= sf::seconds(frametime);
    }
    if (checkCollision() == 1) {
            EndText();
            sleep(5);
        return 1;
    }

    events.MapEventActivator(map, hero);
    for (auto & i : m_enemy) {
        events.MapEventActivator(map, i);
    }
    for (auto & i : m_bulletEnemy) {
        events.MapEventActivator(map, i);
    }
    for (auto & i : m_bullet) {
        events.MapEventActivator(map, i);
    }
    return 0;
}
void Game::LoadMap () {
    map.LoadFromFile(MAP_PLACE);
}
void Game::BaseCamera() {
    m_window.BaseView(PlayerCamera);
}

void Game::Fire(float attack) {
    SpriteInfo si = getBulletInfo();
    m_bullet.resize(m_bullet.size() + 1);
    auto i = m_bullet.end();
    --i;
    i->Setup(hero.getX(), hero.getY(), si.height, si.weight, si.speed, si.attack + attack, si.rate, si.health, si.placeSprite);
    i->dir = hero.viewDir;
}

void Game::FireEnemy(float x, float y, float attack) {
    SpriteInfo si = getBulletInfo();
    m_bulletEnemy.resize(m_bulletEnemy.size() + 1);
    auto i = m_bulletEnemy.end();
    --i;
    i->Setup(x, y, si.height, si.weight, si.speed, si.attack + attack, si.rate, si.health, si.placeSprite);

    float dx = i->getX() - hero.getX();
    float dy = i->getY() - hero.getY();

    float gip = sqrt(dx * dx + dy * dy);

    i->sin = dy / gip;
    i->cos = dx / gip;
}

void Game::MoveHero(float frametime) {
    for (auto it = m_pharmace.begin(); it != m_pharmace.end(); ++it) {
        if (CollisionEntity(*it, hero)) {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
                float s = hero.getHealth() + it->getHealth();
                if (s > 100) {
                    s = 100;
                }
                hero.setHP(s);
                it = m_pharmace.erase(it);
            }
        }
    }

    for (auto it = m_weapon.begin(); it != m_weapon.end(); ++it) {
        if (CollisionEntity(*it, hero)) {
            if (sf::Keyboard::isKeyPressed(sf::Keyboard::E)) {
                if (!hero.active) {
                    hero.setActive(*it);
                    maxWeaponHp = hero.getActive().getHealth();
                    it = m_weapon.erase(it);
                }
            }
        }
    }

    if (hero.active && sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
        m_weapon.resize(m_weapon.size() + 1);
        auto iter = m_weapon.end();
        --iter;
        *iter = hero.getActive();
        iter->getSprite().setPosition(hero.getX(), hero.getY());

        hero.throwItem();
    }
    if(hero.active) {
    }
    float speed = hero.Move(map);
    ++hero.DistBull;
    float attack = hero.getAttack();
    float rate = hero.getRate();
    if (hero.View(m_window)) {
        if (hero.active) {
            attack = hero.getActive().getAttack();
            rate = hero.getActive().getRate();
        }
        if (hero.DistBull > rate) {
            if (hero.active) {
                hero.getActive().setHP(hero.getActive().getHealth() - 1);
            }
            Fire(attack);
            if (hero.getActive().getHealth() <= 0) {
                hero.active = false;
            }
            hero.DistBull -= rate;
        }
    } else {
        if (hero.DistBull > 2 * rate) {
            hero.DistBull -= rate;
        }
    }
    ++hero.frame;
    if (hero.frame >= 36) {
        hero.frame -= 36;
    }
    hero.getSprite().setTextureRect(sf::IntRect((int) hero.getWeight() * (int) (hero.frame / 9),
                                                hero.viewDir * (int) hero.getHeight(),
                                                (int) hero.getWeight(),
                                                (int) hero.getHeight()));
    CollisionPro(hero);
    hero.update(frametime, speed);
}
void Game::SetCam (){
    PlayerCamera.SetCamera(m_window.GetWindowSize());
}
void Game::MoveCam() {
    PlayerCamera.CameraFollow(hero.getX(),hero.getY(), m_window.GetWindowSize(), map.getWidth(), map.getHeight());
}
void Game::Render(){
    m_window.BeginDraw();
    m_window.DrawMap(map.layers);
    for (auto & it : m_bullet) {
        m_window.Draw(it.getSprite());
    }
    if (waveCount != -1) {

        for (auto & it : m_weapon) {
            if (!it.entityCollision) {
                m_window.Draw(it.getSprite());
            }
        }
        for (auto & it : m_pharmace) {
            if (!it.entityCollision)
                m_window.Draw(it.getSprite());
        }
        for (auto & it : m_enemy) {
            m_window.Draw(it.getSprite());
        }
        for (auto & it : m_bulletEnemy) {
            m_window.Draw(it.getSprite());
        }
    }

    if (!m_isWave) {
        TextWave();
        // std::cout << (int)m_beforeWave.asSeconds() << "\n";
    }
    Text();
    Backpack back;
    back.BackpackDraw(GetWindow()->m_window);
    LifeBar HpBar;

    HpBar.HpUpdate(hero.getHealth(), GetWindow()->m_window);
    HpBar.HpDraw(GetWindow()->m_window);

    if (hero.active) {
        Sprite gun;
        gun = hero.getActive().getSprite();
        gun.setScale(2,2);
        gun.setPosition(GetWindow()->m_window.getView().getCenter().x - GetWindow()->m_window.getView().getSize().x / 2 + 140, (GetWindow()->m_window.getView().getCenter().y - GetWindow()->m_window.getView().getSize().y / 2 + 900));
        GetWindow()->m_window.draw(gun);
    }

    m_window.Draw(hero.getSprite());
    m_window.EndDraw();
}

myWindow *Game::GetWindow() {
    return &m_window;
}

sf::Time Game::GetElapsed() { return m_elapsed; }

void Game::RestartClock() {
        sf::Time time = m_clock.restart();
        if (!m_isWave) {
            m_beforeWave -= time;
        }
        m_elapsed += time;
        if (waveCount != -1) {
            m_period += time;
        }
}

Level *Game::GetMap() {
    return &map;
}

Player *Game::GetPlayer() {
    return &hero;
}

void Game::MoveEnemy(float frametime) {
    float heroX = hero.getX();
    float heroY = hero.getY();

    for (auto & it : m_enemy) {
        ++it.DistBull;
        CollisionPro(it);
        float speed = it.move(heroX, heroY); // Надо передать еще одну характеристику - рейндж оружия
        if (speed == -1) {
            if (it.DistBull > it.getRate()) {
                FireEnemy(it.getX(), it.getY(), it.getAttack());
                it.DistBull -= it.getRate();

            }
        } else {
            if (it.DistBull > 2 * it.getRate()) {
                it.DistBull -= it.getRate();
            }
        }
        it.update(frametime, speed);
    }
}

void Game::CollisionPro(Entity &entity) {
    if (!entity.entityCollision) {
        entity.canW = entity.canA = entity.canS = entity.canD = true;
    } else {
        switch (entity.dir) {
            case 1:
                if (entity.canD && entity.canS && entity.canW) {
                    entity.canA = false;
                }
            case 2:
                if (entity.canA && entity.canS && entity.canW) {
                    entity.canD = false;
                }
            case 3:
                if (entity.canA && entity.canS && entity.canD) {
                    entity.canW = false;
                }
            case 0:
                if (entity.canA && entity.canD && entity.canW) {
                    entity.canS = false;
                }
        }
    }
}
void Game::MoveBullet(float frametime) {
    for (auto i = m_bullet.begin(); i != m_bullet.end(); ++i) {
        i->getSprite().setTextureRect(sf::IntRect(0,
                                                0,
                                               (int) i->getWeight(),
                                               (int) i->getHeight()));
        i->update(frametime, i->getSpeed());
    }
}

int Game:: menu(RenderWindow & window, int &key) {
    Vector2f center = window.getView().getCenter();
    Vector2f size = window.getView().getSize();

    Texture menuTexture1, menuTexture2, menuTexture3, menuBackground;
    menuTexture1.loadFromFile("../image/1m.png");
    menuTexture2.loadFromFile("../image/2m.png");
    menuTexture3.loadFromFile("../image/3m.png");
    menuBackground.loadFromFile("../image/back.png_");
    Sprite menu1(menuTexture1), menu2(menuTexture2), menu3(menuTexture3), menuBg(menuBackground);
    int menuNum = -1;
    bool IsMenu = true;
    int x = 725 ;
    menu1.setPosition(center.x - size.x / 2 + 725, center.y - size.y / 2 + 110);
    menu2.setPosition(center.x - size.x / 2 + 725, center.y - size.y / 2 + 400);
    menu3.setPosition(center.x - size.x / 2 + 725, center.y - size.y / 2 + 691);
    menuBg.setPosition(center.x - size.x / 2,center.y - size.y / 2);
    window.clear();

    //////////////////////////////МЕНЮ///////////////////
    while (IsMenu)
    {
        Event event;
        while (window.pollEvent(event)) {
            if (event.type == Event::Closed)
                window.close();
        }

        menu1.setColor(Color::White);
        menu2.setColor(Color::Black);
        menu3.setColor(Color::White);
        menuNum = 0;

        if (IntRect(x, 110, 426, 173).contains(Mouse::getPosition(window))) {
            menu1.setColor(sf::Color::Black);
            menuNum = 1;
        }

        if (key == 1) {
            menu2.setColor(Color::White);
            if (IntRect(x, 400, 426, 173).contains(Mouse::getPosition(window))) {
                menu2.setColor(sf::Color::Black);
                menuNum = 2;
            }
        }

        if (IntRect(x, 650, 426, 173).contains(Mouse::getPosition(window))) {
            menu3.setColor(sf::Color::Black);
            menuNum = 3;
        }

        if (sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
            if (menuNum == 2) { IsMenu = false; m_clock.restart(); } //если нажали первую кнопку, то выходим из меню
            if (menuNum == 3) { window.close(); exit(0); }
            if (menuNum == 1) {
                key = 2;
                m_clock.restart();
                return -1;
            }

        }

        window.draw(menuBg);
        window.draw(menu1);
        window.draw(menu2);
        window.draw(menu3);
        window.display();

    }
}

int Game::checkCollision() {
    for (auto it = m_bullet.begin(); it != m_bullet.end(); ++it) {
        if (it->getX() < 0 || it->getX() > map.getWidth() ||
                it->getY() < 0 || it->getY() > map.getHeight() ||
                it->entityCollision) {
            it = m_bullet.erase(it);
            continue;
        }

        for (auto i = m_enemy.begin(); i != m_enemy.end(); ++i) {
            if (CollisionEntity(*it, *i)) {
                i->setHP(i->getHealth() - it->getAttack());
                if (i->getHealth() <= 0) {
                    i = m_enemy.erase(i);
                }

                it = m_bullet.erase(it);
                break;
            }
        }
    }


    for (auto it = m_bulletEnemy.begin(); it != m_bulletEnemy.end(); ++it) {
        if (it->getX() < 0 || it->getX() > map.getWidth() ||
                it->getY() < 0 || it->getY() > map.getHeight() ||
                it->entityCollision) {
            it = m_bulletEnemy.erase(it);
            continue;
        }

        if (CollisionEntity(*it, hero)) {
            hero.setHP(hero.getHealth() - it->getAttack());
            if (hero.getHealth() <= 0) {
                return 1;
            }

            it = m_bulletEnemy.erase(it);
        }

    }
    return 0;
}

void Game::updateBulletEnemy(float frametime) {
    for (auto it = m_bulletEnemy.begin(); it != m_bulletEnemy.end(); ++it) {
        float dx = it->cos * it->getSpeed() * frametime;
        float dy = it->sin * it->getSpeed() * frametime;
        it->setPosition(it->getX() - dx, it->getY() - dy);
        it->getSprite().setPosition(it->getX(), it->getY());
    }
}
void Game:: Text() {
    sf :: Font font;
    font.loadFromFile("../text.ttf");
    sf :: Text text("", font, 35);
    text.setColor(Color::Black);
    std::ostringstream Counter;
    Counter << waveCount;
    text.setString("Wave counter: " + Counter.str());
    text.setPosition(GetWindow()->m_window.getView().getCenter().x + 650, GetWindow()->m_window.getView().getCenter().y - 520);

    sf :: Text text2("", font, 35);
    text2.setColor(Color::Black);
    std::ostringstream CounterEnemy;
    CounterEnemy << m_enemy.size();
    text2.setString("Enemies left: " + CounterEnemy.str());
    text2.setPosition(GetWindow()->m_window.getView().getCenter().x + 650, GetWindow()->m_window.getView().getCenter().y - 470);

    if (hero.active) {
        sf::Text text3("", font, 30);
        text3.setColor(Color::Black);
        std::ostringstream CounterHp;
        std::ostringstream MaxHp;
        if (maxWeaponHp == 0) {
            CounterHp << 0;
        } else {
            CounterHp << hero.getActive().getHealth();
        }

        text3.setString("Bullets: " + CounterHp.str());
        text3.setPosition(GetWindow()->m_window.getView().getCenter().x - 820,
                          GetWindow()->m_window.getView().getCenter().y + 270);

        GetWindow()->m_window.draw(text3);
    }
    GetWindow()->m_window.draw(text2);
    GetWindow()->m_window.draw(text);
}

void Game:: EndText() {
    sf :: Font font;
    font.loadFromFile("../End.ttf");
    sf :: Text text("", font, 220);
    text.setColor(Color::Red);
    std::ostringstream Counter;
    Counter << waveCount - 1;
    text.setString("GAME OVER \nPassed waves:" + Counter.str());
    text.setPosition(GetWindow()->m_window.getView().getCenter().x - 440, GetWindow()->m_window.getView().getCenter().y - 430);
    GetWindow()->m_window.draw(text);
    GetWindow()->m_window.display();
    GetWindow()->m_window.clear();
}

void Game:: TextWave() {
    sf :: Font font;
    font.loadFromFile("../text.ttf");
    sf :: Text text("", font, 35);
    text.setColor(Color::Red);
    std::ostringstream Counter;
    Counter << (int)m_beforeWave.asSeconds();
    text.setString("next wave:  " + Counter.str());
    text.setPosition(GetWindow()->m_window.getView().getCenter().x - 100, GetWindow()->m_window.getView().getCenter().y - 520);

    GetWindow()->m_window.draw(text);
}