#include "Level.h"
#include "ObjectEventManager.h"
#include "Collision.h"
#include "Game.h"
//
// Created by felixdev on 29.04.2020.
//
void MapEvent::CollisionActivator (Level &map, Entity &player) {
    std::vector<Object> CollisionObjects = map.GetObjectForName("collision");
    player.setCollision(Collision(player.getRect(),CollisionObjects));
}

void MapEvent::MapEventActivator(Level &map, Entity &player) {
    this->CollisionActivator (map, player);
}