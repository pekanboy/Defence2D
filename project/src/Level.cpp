//
// Created by felixdev on 27.04.2020.
//
#include <iostream>
#include "Level.h"
#include "Common.h"
#include "../../Libraries/TinyXML/tinyxml.h"

bool Level::LoadFromFile(std::string filename)
{
    TiXmlDocument levelFile(filename.c_str());
    if(!levelFile.LoadFile())
    {
        std::cout << "LOADING ERROR" << std::endl;
        return false;
    }
    int i = 0;
    TiXmlElement *map;
    map = levelFile.FirstChildElement("map");
    width = atoi(map->Attribute("width"));
    height = atoi(map->Attribute("height"));
    tileWidth = atoi(map->Attribute("tilewidth"));
    tileHeight = atoi(map->Attribute("tileheight"));
    TiXmlElement *tilesetElement;
    tilesetElement = map->FirstChildElement("tileset");
    firstTileID = atoi(tilesetElement->Attribute("firstgid"));
    TiXmlElement *image;
    image = tilesetElement->FirstChildElement("image");
    std::string imagepath = image->Attribute("source");
    sf::Image img;
    if(!img.loadFromFile(MAP_TILESET_FILE))
    {
        std::cout << "Failed to load tile sheet." << std::endl;
        return false;
    }
    img.createMaskFromColor(sf::Color(109, 159, 185));
    tilesetImage.loadFromImage(img);
    tilesetImage.setSmooth(false);
    int columns = tilesetImage.getSize().x / tileWidth;
    int rows = tilesetImage.getSize().y / tileHeight;
    std::vector<sf::Rect<int>> subRects;
    for(int y = 0; y < rows; y++)
        for(int x = 0; x < columns; x++)
        {
            sf::Rect<int> rect;
            rect.top = y * tileHeight;
            rect.height = tileHeight;
            rect.left = x * tileWidth;
            rect.width = tileWidth;
            subRects.push_back(rect);
        }
    TiXmlElement *layerElement;
    layerElement = map->FirstChildElement("layer");
    while(layerElement)
    {
        Layer layer;
        TiXmlElement *layerDataElement;
        layerDataElement = layerElement->FirstChildElement("data");
        TiXmlElement *tileElement;
        tileElement = layerDataElement->FirstChildElement("tile");
        int x = 0;
        int y = 0;
        while(tileElement)
        {
            int tileGID = atoi(tileElement->Attribute("gid"));
            int subRectToUse = tileGID - firstTileID;
            if (subRectToUse >= 0)
            {
                sf::Sprite sprite;
                sprite.setTexture(tilesetImage);
                sprite.setTextureRect(subRects[subRectToUse]);
                sprite.setPosition(x * tileWidth, y * tileHeight);
                sprite.setColor(sf::Color(255, 255, 255, 255));

                layer.tiles.push_back(sprite);
            }

            tileElement = tileElement->NextSiblingElement("tile");

            x++;
            if (x >= width)
            {
                x = 0;
                y++;
                if(y >= height)
                    y = 0;
            }
        }
        layers.push_back(layer);
        layerElement = layerElement->NextSiblingElement("layer");
    }
    TiXmlElement *objectGroupElement;
    std::string objectTypeName;
    if (map->FirstChildElement("objectgroup") != NULL)
    {
        objectGroupElement = map->FirstChildElement("objectgroup");
        objectTypeName =  objectGroupElement->Attribute("name");
        while (objectGroupElement)
        {
            TiXmlElement *objectElement;
            objectElement = objectGroupElement->FirstChildElement("object");
            while(objectElement)
            {
                int x = atoi(objectElement->Attribute("x"));
                int y = atoi(objectElement->Attribute("y"));

                int width, height;

                sf::Sprite sprite;
                sprite.setTexture(tilesetImage);
                sprite.setTextureRect(sf::Rect<int>(0,0,0,0));
                sprite.setPosition(x, y);

                if (objectElement->Attribute("width") != NULL)
                {
                    width = atoi(objectElement->Attribute("width"));
                    height = atoi(objectElement->Attribute("height"));
                }
                else
                {
                    width = subRects[atoi(objectElement->Attribute("gid")) - firstTileID].width;
                    height = subRects[atoi(objectElement->Attribute("gid")) - firstTileID].height;
                    sprite.setTextureRect(subRects[atoi(objectElement->Attribute("gid")) - firstTileID]);
                }
                Object objectRect;
                objectRect.rectObj.top = y;
                objectRect.rectObj.left = x;
                objectRect.rectObj.height = height;
                objectRect.rectObj.width = width;
                objectRect.nameObj = objectTypeName;
                objects.push_back(objectRect);
                i++;
                objectElement = objectElement->NextSiblingElement("object");
            }
            objectGroupElement = objectGroupElement->NextSiblingElement("objectgroup");
        }
    }
    return true;
}

std::vector<Object> Level::GetObjectForName(std::string name)
{
    std::vector<Object> CollisionObject;
    for (int i = 0; i < objects.size(); i++) {
        if (objects[i].nameObj == "collision") {
            CollisionObject.push_back(objects[i]);
        }
    }
    return CollisionObject;
}

int Level::getWidth() {
    return width * tileWidth;
}

int Level::getHeight() {
    return height * tileHeight;
}
