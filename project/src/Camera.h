//
// Created by felixdev on 27.04.2020.
//

#ifndef DEFENCE2D_CAMERA_H
#define DEFENCE2D_CAMERA_H

#include <SFML/Graphics.hpp>

using namespace sf;

class CameraMan {
private:
    sf::View PlayerView;
public:
    void CameraFollow(float x, float y, sf::Vector2u size, int w, int h);
    void SetCamera(sf::Vector2u size);
    sf::View GetPlayerCamera();
};

#endif //DEFENCE2D_CAMERA_H
