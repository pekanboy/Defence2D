//
// Created by felixdev on 29.04.2020.
//

#ifndef DEFENCE2D_OBJECTEVENTMANAGER
#define DEFENCE2D_OBJECTEVENTMANAGER

#include "Entity.h"

class MapEvent {
private:
    void CollisionActivator(Level &map, Entity &player);
public:
    void MapEventActivator(Level &map, Entity &player);
};

#endif // DEFENCE2D_OBJECTEVENTMANAGER
