//
// Created by aleksey on 27.04.2020.
//

#ifndef DEFENCE2D_CONFIGURATIONCLASS_H
#define DEFENCE2D_CONFIGURATIONCLASS_H
#include <string>
#include <vector>
#include <iostream>
#include "../../Libraries/TinyXML/tinyxml.h"
#include "Common.h"

class SpriteInfo {
public:
    float weight;
    float height;
    float speed;
    float attack;
    float rate;
    float health;
    std::string placeSprite;

    SpriteInfo& operator=(const SpriteInfo &spriteInfo) {
        if (this != &spriteInfo) {
            weight = spriteInfo.weight;
            height = spriteInfo.height;
            speed = spriteInfo.speed;
            attack = spriteInfo.attack;
            placeSprite = spriteInfo.placeSprite;
        }
        return *this;
    }
};

static SpriteInfo getPlayerInfo() {
    SpriteInfo playerSprite;
    TiXmlDocument doc(SPRITE_CONFIG);
    if (!doc.LoadFile()) {
        std::cout << "! Failed loading spriteInfo.xml." << std::endl;
    }
    TiXmlElement *element = doc.FirstChildElement("player");

    playerSprite.weight = atoi(element->Attribute("weight"));
    playerSprite.height = atoi(element->Attribute("height"));
    playerSprite.speed = atoi(element->Attribute("speed"));
    playerSprite.placeSprite = element->Attribute("placeSprite");
    playerSprite.attack = atoi(element->Attribute("attack"));
    playerSprite.rate = atoi(element->Attribute("rate"));
    playerSprite.health = atoi(element->Attribute("health"));

    return playerSprite;
}

static std::vector<SpriteInfo> getPharmaceInfo() {
    std::vector<SpriteInfo> pharmaceSprite;
    TiXmlDocument doc(SPRITE_CONFIG);
    if (!doc.LoadFile()) {
        std::cout << "! Failed loading spriteInfo.xml." << std::endl;
    }
    TiXmlElement *element = doc.FirstChildElement("pharmace");

    int count = atoi(element->Attribute("count"));
    float weight = atoi(element->Attribute("weight"));
    float height = atoi(element->Attribute("height"));
    std::string placeSprite = element->Attribute("placeSprite");
    for (int j = 0; j < count; ++j) {
        SpriteInfo i;
        srand(time(nullptr));
        i.speed = 0;
        i.attack = 0;
        i.rate = 0;
        i.weight = weight;
        i.height = height;
        i.placeSprite = placeSprite;
        i.health = (rand() % 5 + 1) * 10;

        pharmaceSprite.push_back(i);
    }

    return pharmaceSprite;
}

static SpriteInfo getBulletInfo() {
    SpriteInfo BulletSprite;
    TiXmlDocument doc(SPRITE_CONFIG);
    if (!doc.LoadFile()) {
        std::cout << "! Failed loading spriteInfo.xml." << std::endl;
    }
    TiXmlElement *element = doc.FirstChildElement("bullet");
    BulletSprite.weight = atoi(element->Attribute("weight"));
    BulletSprite.height = atoi(element->Attribute("height"));
    BulletSprite.speed = atoi(element->Attribute("speed"));
    BulletSprite.placeSprite = element->Attribute("placeSprite");
    BulletSprite.attack = atoi(element->Attribute("attack"));
    BulletSprite.rate = atoi(element->Attribute("rate"));
    BulletSprite.health = atoi(element->Attribute("health"));
    return BulletSprite;
}

static std::vector<SpriteInfo> getEnemyInfo(const char *waveNum) {
    if (waveNum == nullptr) {
        std::vector<SpriteInfo> si;
        return si;
    }
    std::vector<SpriteInfo> EnemySprite;
    TiXmlDocument doc(SPRITE_CONFIG);
    if (!doc.LoadFile()) {
        std::cout << "! Failed loading spriteInfo.xml." << std::endl;
    }

    TiXmlElement *element = doc.FirstChildElement("wave");
    TiXmlElement *countWave = element->FirstChildElement(waveNum);
    TiXmlElement *statEnemy = countWave->FirstChildElement("enemy");

    while(statEnemy != nullptr) {
        SpriteInfo si;
        si.weight = atoi(statEnemy->Attribute("weight"));
        si.height = atoi(statEnemy->Attribute("height"));
        si.speed = atoi(statEnemy->Attribute("speed"));
        si.placeSprite = statEnemy->Attribute("placeSprite");
        si.attack = atoi(statEnemy->Attribute("attack"));
        si.rate = atoi(statEnemy->Attribute("rate"));
        si.health = atoi(statEnemy->Attribute("health"));

        EnemySprite.push_back(si);

        statEnemy = statEnemy->NextSiblingElement("enemy");
    }

    return EnemySprite;
}

static std::vector<SpriteInfo> getItemInfo(const char *waveNum) {
    if (waveNum == nullptr) {
        std::vector<SpriteInfo> si;
        return si;
    }
    std::vector<SpriteInfo> ItemSprite;
    TiXmlDocument doc(SPRITE_CONFIG);
    if (!doc.LoadFile()) {
        std::cout << "! Failed loading spriteInfo.xml." << std::endl;
    }

    TiXmlElement *element = doc.FirstChildElement("items");
    TiXmlElement *countWave = element->FirstChildElement(waveNum);
    TiXmlElement *statItem = countWave->FirstChildElement("weapon");

    while(statItem != nullptr) {
        SpriteInfo si;
        si.weight = atoi(statItem->Attribute("weight"));
        si.height = atoi(statItem->Attribute("height"));
        si.speed = 0;
        si.placeSprite = statItem->Attribute("placeSprite");
        si.attack = atoi(statItem->Attribute("attack"));
        si.rate = atoi(statItem->Attribute("rate"));
        si.health = atoi(statItem->Attribute("health"));

        ItemSprite.push_back(si);

        statItem = statItem->NextSiblingElement("weapon");
    }

    return ItemSprite;
}

#endif // DEFENCE2D_CONFIGURATIONCLASS_H