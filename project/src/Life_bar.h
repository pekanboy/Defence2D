//
// Created by nikita on 15.05.2020.
//

#ifndef DEFENCE2D_LIFE_BAR_H
#define DEFENCE2D_LIFE_BAR_H

#include <SFML/Graphics.hpp>
using namespace sf;

class LifeBar
{
private:
    Image image;
    Texture t;
    Sprite s;
    int max;
    RectangleShape bar;
public:
    void HpUpdate(int, RenderWindow &window);
    LifeBar();
    void HpDraw(RenderWindow &);
};

#endif  //  DEFENCE2D_LIFE_BAR_H