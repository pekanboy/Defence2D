//
// Created by aleksey on 12.05.2020.
//

#ifndef DEFENCE2D_WEAPON_H
#define DEFENCE2D_WEAPON_H

#include "Entity.h"

class Weapon : public Entity {
public:
    Weapon() = default;
    Weapon(float x, float y, float h, float w, float speed, float attack, float rate, float health, const sf::String &name);
    Weapon& operator=(const Weapon &entity);

    ~Weapon() override {}
};

#endif //DEFENCE2D_WEAPON_H
