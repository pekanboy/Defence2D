//
// Created by aleksey on 12.05.2020.
//

#include <iostream>
#include "weapon.h"

Weapon::Weapon(float x, float y, float h, float w, float speed, float attack, float rate, float health, const sf::String &name) {
    Setup(x, y, h, w, speed, attack, rate, health, name);
}

Weapon& Weapon::operator=(const Weapon &entity) {
    if (this != &entity)
        Setup(entity.getX(), entity.getY(), entity.getHeight(), entity.getWeight(), entity.getSpeed(), entity.m_attack, entity.getRate(),  entity.getHealth(), entity.getName());
    return *this;
}

