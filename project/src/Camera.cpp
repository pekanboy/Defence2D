//
// Created by felixdev on 27.04.2020.
//
#include "Camera.h"

void CameraMan::CameraFollow (float x, float y, sf::Vector2u size, int w, int h) {
    float cam_x = x, cam_y = y;
    if (cam_x < size.x / 2) cam_x = size.x / 2;
    if (cam_y < size.y / 2) cam_y = size.y / 2;
    if (cam_y > w - size.y / 2) cam_y = w - size.y / 2;
    if (cam_x > h - size.x /2 ) cam_x = h - size.x / 2;
    PlayerView.setCenter(cam_x,cam_y);
}

void CameraMan::SetCamera(sf::Vector2u size) {
    PlayerView.setSize(size.x, size.y);
    PlayerView.zoom(1.0f);
}

sf::View CameraMan::GetPlayerCamera() {
    return PlayerView;
}