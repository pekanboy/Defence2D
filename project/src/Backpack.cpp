//
// Created by NIKITA on 17.05.2020.
//

#include "Backpack.h"

void Backpack :: BackpackDraw(RenderWindow &window){
    Vector2f center = window.getView().getCenter();
    Vector2f size = window.getView().getSize();


    image.loadFromFile("../image/backpack.png");
    //image.createMaskFromColor(Color :: White);
    pic.loadFromImage(image);
    Sprite back(pic);
    back.setPosition(center.x - size.x / 2 + 50, center.y - size.y / 2 + 850);

    window.draw(back);
}