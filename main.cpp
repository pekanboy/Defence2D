#include <iostream>
#include <sstream>
#include "project/src/Game.h"
#include "project/src/ObjectEventManager.h"

int main(int argc, char **argv) {
    myWindow  window(HEADER_NAME, sf::Vector2u(1920, 1080));
    int key = 0;
    NewGame :
    Game game(window);
    if (key != 2) {
        key = 0;
        game.menu(game.GetWindow()->m_window, key);
    }
    key = 1;
    game.VSync();
    game.LoadMap();
    game.SetCam();

    while (!game.GetWindow()->IsDone()) {
        if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
            int m = game.menu(game.GetWindow()->m_window, key);
            if (m == -1) {
                goto NewGame;
            }
        }
        game.BaseCamera();
        if (game.Update() == 1) {
            goto NewGame;
        }
        game.Render();
        game.MoveCam();
        game.RestartClock();
    }
}